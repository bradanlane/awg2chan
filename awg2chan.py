''' ************************************************************************************
* File:    2 Channel Arbitrary Waveform Generator (AWG) awg2chan.py
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
*
* Bradán Lane - 2023.10.24
*   2 channel independent waveform generator
*   complete refactoring of code into Waveform class
*   API
*   documentation
*   to be honest, there is almost no original code left
*
* n16k6 - 2022.09.14
*     use dual DMA to shift out squarewave
*     https://github.com/n19k6/squarewave/v11_honda_v1/03_circuit_r2r/dma_1.py
*
* Martin Fishcer(hoiho) 2021.02.20
      base DMA channel code
      https://github.com/hoihu/projects/blob/master/pico/dma.py
*
* ************************************************************************************ '''

''' ---

# AWG 2 Channel Arbitrary Waveform Generator

An 'arbitrary waveform generator' or AWG is able to generate a range of waveforms.
This code uses the Raspberry Pi PICO and its PIO, state machines, and DMA to
provide two completely independent waveforms.

Each waveform generates values between 0v and VCC (3.3V on the PICO).
It is possible to add a combination of voltage dividers and an opamp
to provide larger or smaller ranges or make the output a +/- range.

**IMPORTANT:** This is for educational purposes only.
The accuracy of the generated frequencies may not be sufficient.

Testing results: 
- sampling buffer size = 4096 has an error of +/- 0.50 (with a max error of 0.75)
- sampling buffer size = 8192 has an error of +/- 0.25 (with a max error of 0.50)

Of course, you are welcome to try and improve it.
If you find improvements, you are encouraged to submit a pull-request.
Thank you.

--------------------------------------------------------------------------

--- '''

''' ---
--------------------------------------------------------------------------
## Functionality

The 'arbitrary waveform generator' or AWG is able to generate a range of waveforms.
Each waveform has zero or more properties.
The waveform is independent of the frequency.
This allows for the waveform to be created and reused at difference frequencies.
--- '''

''' ---
--------------------------------------------------------------------------
## Hardware

The `awg2chan.py` code has been written for and tested with the Raspberry Pi PICO.
It is possible to use other PR2040 boards provided enough sequential GPIO exposed.
The board must have two sets of 8 sequential GPIO pins suitable for output.
Each sequence of pins becomes an 8-bit DAC through the creation of an R-2R ladder.

> An R–2R ladder configuration is a simple and inexpensive way to perform digital-to-analog conversion (DAC),
> using repetitive arrangements of precise resistor networks in a ladder-like configuration.

The most compact R-2R for the PICO uses 0603 SMD resistors which are soldered directly onto the board.
The test hardware uses 1KOhm 1% 0603 resistors.

<div align="center">![PICO with R-2R DACs](files/awg2chan_large.png){width=75%}<br/>
click for larger image<br/><br/></div>

## Software

The `awg2chan.py` is written in MicroPython.
The Raspberry Pi PICO requires the MicroPython [UF2](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html#drag-and-drop-micropython) installed.

If you wish to have `awg2chan.py` run automatically every time the PICO is powered, it needs to be installed onto the PICO.
Copy `awg2chan.py` to the PICO (most people use Thonny or ampy) and name the file `main.py`.

### Architecture

There are multiple steps to go from waveform to DAC output:

- the mathematical waveform is sliced into 'n' sample values _(where 'n' is a defined constant for each channel) and stored in a buffer
- chained DMA channels are initialized for the buffer
  - channel 1 chunks the sample buffer into 32 bit blocks and loads one block at a time to channel 0
  - channel 0 loads 8 bits at a time into the PIO FIFO
  - this process repeats indefinitely
- the PIO takes 8 bits at a time and set the 8 consecutive GPIO pins high or low according the bits as 0 or 1
- the R-2R latter combines the GPIO pins hi/lo into an analog voltage

At any time, a new waveform may be sliced into a sample buffer.
To reduce the delay, there are two samples buffers per channel.

There are 2 independent channels which results in a total of 4 sampling buffers.

The sampling size effects the quality of the analog waveform and is dependent of the usable frequency range.
A larger sampling buffer is needed to form reasonable waveforms at higher frequencies,
while a smaller sampling buffer is useable for waveforms in the audio range.
--- '''

from utime import sleep, ticks_ms
from uctypes import BF_POS, BF_LEN, BFUINT32, UINT32, struct, addressof
from array import array
from math import floor, sqrt, pi, sin, exp
from random import random # used for making the 'noise' waveform
from sys import print_exception

import rp2
from rp2 import PIO, StateMachine, asm_pio

from machine import Pin, mem32

'''
    We run the PIO at the same clock as the microcontroller. It could be over clocked to 2X.
    The projects uses 8 GPIO for each DAC. It is possible to go to 12 bits bu then a lot of 
    the code will required changing.
    The convenience of using 8-bits per DAC.
    It is possible to change these values but then the related code will need to change too. You have been warned.
'''
PICO_CLOCK_SPEED    = 125000000 # clock frequency of the PICO PIO
#PICO_CLOCK_SPEED   = 250000000 # clock frequency of the PICO PIO is overclocked 2X
DAC_SIZE            = 8         # number of GPIO pins used for each DAC
SAMPLE_PER_32BITS   = 4         # 4 8bit values will fit in a 32bit *BUT* we get better frequency accuracy but not doing that
CLOCK_THRESHOLD		= 1.0		# dependent on SAMPLE_PER_32BITS 1,2,3,4 => threshold 3.0,2.0,1.0,1.0
DMA_XFER_SIZE		= 2			# dependent on SAMPLE_PER_32BITS 1,2,3,4 => DMA 0(8bit), 1(16bit), 2(32bit), 2(32bit)
DEFAULT_SAMPLING_SIZE = 4096    # 1024 if much faster but 4096 has an error of +/- 0.50 to 0.75; 8192 has an error of +/- 0.25 to 0.50

import machine
machine.freq(PICO_CLOCK_SPEED) # set the CPU frequency


'''
    We treat the DMA as a structure and use Python struct(address, struct_definition)
    to give the memory locations convenient names for coding.
    The 'DMA_LAYOUT' is the map of the DMA and the 'DMA_CTRL_REG' is the map of the
    'CTRL_TRIG' within the 'DMA_LAYOUT'
'''
# note: 2 DMA channels are chained together for each DAC - aka channel 0 & 1, channel 2 & 3, etc.
DMA0_BASE           = 0x50000000
DMA_ABORT           = DMA0_BASE + 0x444   # bit field which handles all DMA channels
DMA_CHANNEL_SIZE    = 0x40
DMA_NO_PACING       = 0x3F

# we only use one of the two PIO; we use to of the four state machines of this PIO
PIO0_BASE           = 0x50200000
PIO0_TXF_OFFSET     = 0x10
PIO0_TXF_SIZE       = 0x4
PIO0_CLKDIV_OFFSET  = 0xc8
PIO0_CLKDIV_SIZE    = 0x18
PIO_TX_WAIT         = 0x00


DMA_CTRL_REG = {
    "AHB_ERROR":        31 << BF_POS | 1 << BF_LEN | BFUINT32,
    "READ_ERR":         30 << BF_POS | 1 << BF_LEN | BFUINT32,
    "WRITE_ERR":        29 << BF_POS | 1 << BF_LEN | BFUINT32,
    "Reserved":         25 << BF_POS | 4 << BF_LEN | BFUINT32,
    "BUSY":             24 << BF_POS | 1 << BF_LEN | BFUINT32,
    "SNIFF_EN":         23 << BF_POS | 1 << BF_LEN | BFUINT32,
    "BSWAP":            22 << BF_POS | 1 << BF_LEN | BFUINT32,
    "IRQ_QUIET":        21 << BF_POS | 1 << BF_LEN | BFUINT32,  # do not generate interrupt
    "TREQ_SEL":         15 << BF_POS | 6 << BF_LEN | BFUINT32,  # wait on flag at address offset or 0x3F for 'no pacing'
    "CHAIN_TO":         11 << BF_POS | 4 << BF_LEN | BFUINT32,  # channel number to chain to
    "RING_SEL":         10 << BF_POS | 1 << BF_LEN | BFUINT32,  # 0 or 1 wrapping flag
    "RING_SIZE":         6 << BF_POS | 4 << BF_LEN | BFUINT32,  # wrapping ring size
    "INCR_WRITE":        5 << BF_POS | 1 << BF_LEN | BFUINT32,  # 0 or 1 do incremental write vs single write
    "INCR_READ":         4 << BF_POS | 1 << BF_LEN | BFUINT32,  # 0 or 1 do incremental read vs single read
    "DATA_SIZE":         2 << BF_POS | 2 << BF_LEN | BFUINT32,  # number of 16 bit WORDs aka 32 bits = 2
    "HIGH_PRIO":         1 << BF_POS | 1 << BF_LEN | BFUINT32,  # 0 or 1
    "EN":                0 << BF_POS | 1 << BF_LEN | BFUINT32,
}

DMA_LAYOUT = {
    "READ_ADDR":         0 | UINT32,
    "WRITE_ADDR":        4 | UINT32,
    "TRANS_COUNT":       8 | UINT32,
    "CTRL_TRIG":       (12,  DMA_CTRL_REG),
    "CTRL_TRIG_RAW":    12 | UINT32, # for single update of all fields
    "AL1_CTRL":         16 | UINT32,
}


'''
    -----------------------------------------------------------------------------
    The PIO code is a combination of the `asm_pio` setup and the function
    immediately after.
    To be totally honest, I don't know where this is documented. I only know
    that every example I found, uses this solution.
    -----------------------------------------------------------------------------
'''
# --------------------------------------------------------------------------------
#         fifo_join      = PIO.JOIN_TX,                      # use the entire FIFO for TX (no RX)
@asm_pio(out_init       = (( PIO.OUT_HIGH, ) * DAC_SIZE),
         out_shiftdir   = PIO.SHIFT_RIGHT,                  # shift bits out, i.e. each pin maps to 1 bit
         autopull       = True,                             # get more data when possible
         pull_thresh    = (DAC_SIZE) * SAMPLE_PER_32BITS    # this should be the full 32 bits but can be less
        )
def pio_stream_bits():
    out(pins,8)
# --------------------------------------------------------------------------------


# helper function to clamp a value to a range
# most common usage is `clamp(val, 0, 255)` to clamp to an 8-bit byte
def clamp(n, lower, upper):
    return max( min( upper, n ), lower )



''' ---
--------------------------------------------------------------------------
## API

The API is comprised of 2 classes `Waveform` and `DacChannel`.

The `DacChannel` is the primary class and controls the operations of the two available DACs.
A `DacChannel` object has a `Waveform` which controls the generator function for the output signal.

**NOTE:** a future enhancement is to allow _modifiers_ for waveforms - i.e. mathematically combining waveforms.

--- '''

''' ---
--------------------------------------------------------------------------
### Waveform Class

The `Waveform` class manages the construction, initialization, and slicing of a waveform.

There are 9 available waveforms:
1. [SINE](https://en.wikipedia.org/wiki/Sine_wave)
1. [ASD] (generic attack-sustain-decay)
1. [SQUARE](https://en.wikipedia.org/wiki/Square_wave) (ASD with zero attack and zero decay)
1. [SAWTOOTH1](https://en.wikipedia.org/wiki/Sawtooth_wave) (ASD with zero attack and zero sustain)
1. [SAWTOOTH2](https://en.wikipedia.org/wiki/Sawtooth_wave) (ASD with zero sustain and zero decay)
1. [TRIANGLE](https://en.wikipedia.org/wiki/Triangle_wave) (ASD with zero sustain and uniform attack and decay)
1. [SINC](https://en.wikipedia.org/wiki/Sinc_function)
1. [GAUSSIAN](https://en.wikipedia.org/wiki/Gaussian_function)
1. [EXPONENTIAL](https://en.wikipedia.org/wiki/Exponential_function)
1. [NOISE](https://en.wikipedia.org/wiki/Perlin_noise)

--- '''


class Waveform:
    NONE = 0
    SINE = 1
    ASD = 2
    SQUARE = 3      # special case of ASD
    SAWTOOTH1 = 4   # special case of ASD
    SAWTOOTH2 = 5   # special case of ASD
    TRIANGLE = 6    # special case of ASD
    GAUSSIAN = 7
    SINC = 8
    EXPONENTIAL = 9
    NOISE = 10
    MAX_TYPE = 11

    ''' ---
#### Waveform Constructor
The `Waveform` constructor takes no parameters.
It initializes the public and private properties.
The default wave function is set to `None`.

Usage: `my_wave = Waveform()`

Each waveform has four public properties:
- amplitude - initial waveform amplitude
- phase - initial phase of the waveform
- offset - initial voltage shift of the waveform
- multiplier - a horizontal multiplier (used for fm and am; must be an integer value)
- fm - optional second waveform to modulate the frequency of the base waveform
- am - optional waveform to modulate teh amplitude of the base waveform

Usage: `wave.amplitude = 0.7071`
Usage: `wave1.envelop = wave2`
    --- '''
    def __init__(self):
        self.type = 0               	# index into the waveform types
        self.amplitude = 0.0        	# waveform amplitude (too high a value will be clipped)
        self.offset = 0.0           	# waveform initial Y offset
        self.phase = 0.0            	# waveform starting phase
        self.__shift = 0.0          	# waveform phase shifted dynamically
        self.multiplier = 1         	# multiplier for when mix and envelope ( the base, mix, and envelope have a single frequency and this allows a integer scaler between them)
        self.__function = None      	# waveform generation function
        self.properties = [0.0,0.0,0.0]	# some waveform functions need to store parameters

        self.fm:Waveform = None     # optional waveform to mix with current waveform (modulate the frequency)
        self.am:Waveform = None     # optional waveform to use an an envelop to modify current waveform (modulate the amplitude)



    # the basic wave functions were described by Rolf Oldeman (rgco)
    def __sine(self, sample):
        return sin(sample * 2 * pi)


    def __asd(self, sample):
        # ASD is a generic waveform with various linear properties
        # a ASD has 3 properties: p[0] = rise, p[1] = sustain, and p[2] = decay
        # special cases of ASD are: square, sawtooth, and triangle
        attack = self.properties[0]
        sustain = self.properties[1]
        decay = self.properties[2]
        if sample < attack:
            if attack == 0.0: return 0.0    # avoid div by 0
            return sample / attack
        if attack < sample < (attack + sustain):
            return 1.0
        if sample < (attack + sustain + decay):
            if decay == 0.0: return 0.0     # avoid div by 0
            return 1.0 - (sample - (attack + sustain)) / decay
        return 0.0


    def __exponential(self, sample):
        # the exponential wave has a single property: p[0] = width
        width = self.properties[0]
        return exp(-sample / width)


    def __noise(self, sample):
        # the noise function has a single property: p[0] = quality (where 1 is uniform and >10 is gaussian)
        quality = self.properties[0]
        return sum([random() - 0.5 for _ in range(quality)]) * sqrt(12 / quality)


    def __gaussian(self, sample):
        # the gaussian wave has a single property: p[0] = width
        width = self.properties[0]
        return exp(-((sample - 0.5) / width) ** 2)


    def __sinc(self, sample):
        # the sinc wave (not sine wave) has a single property: p[0] = width
        if sample == 0.5:
            return 1.0
        width = self.properties[0]
        return sin((sample - 0.5) / width) / ((sample - 0.5) / width)



    ''' ---
#### set() Method
The `wave.set()` method takes two parameters:
- the type constants (e.g. `Waveform.PULSE`)
- an optional `data=value` or `data=list` parameter (lists must contain the required number of values for the waveform)
    --- '''
    def set(self, type, data=[]):
        self.type       = type
        self.amplitude  = 1.0
        self.offset     = 0.0
        self.phase      = 0.0
        self.multiplier = 1
        self.__shift    = 0.0

        if type == Waveform.SINE:
            ''' ---
##### Sine Wave Function
A sine wave function. Has no additional properties

The sine wave is most useful as a waveform and can be used as fm or am

Usage: `wave.set`(Waveform.SINE, None)`
            --- '''
            self.amplitude = 0.5
            self.offset = 0.5
            self.__function = self.__sine
            self.properties = [0.0,0.0,0.0]
            pass
        elif type == Waveform.ASD:
            ''' ---
##### ASD Wave Function
An ASD wave is a generic angular waveform with three possible stages:
- attack - the fractional time spent transitioning for 0.0 to `amplitude`
- sustain - the fractional time spent at `amplitude`
- decay - the fractional time spent transitioning from `amplitude` to 0.0

The ASD is most useful for am

Usage: `wave.set`(Waveform.PULSE, [0.25, 0.25, 0.5])`
            --- '''
            self.__function = self.__asd
            if not data or not (isinstance(data, list) and (len(data) == 3)): data = [0.33, 0.34, 0.33]
            self.properties = data
            pass
        elif type == Waveform.SQUARE:
            ''' ---
##### Square Wave Function
A square wave is a special case of a ASD wave where both attack and decay are 0.0
and the sustain value defines the duty cycle of the square wave.

The square is useful as a waveform, am, and fm

Usage: `wave.set`(Waveform.SQUARE, 0.66)`
            --- '''
            self.__function = self.__asd
            if    data and (isinstance(data, int) or isinstance(data, float)): data = [0.0, data, 0.0]
            elif  data and  isinstance(data, list) and len(data) == 1: data = [0.0, data[0], 0.0]
            else: data = [0.0, 0.5, 0.0]   # default to a full attack
            self.properties = data
            pass
        elif type == Waveform.SAWTOOTH1:
            ''' ---
##### Sawtooth1 Wave Function
A sawtooth wave is a special case of a ASD wave where only decay has a non-zero value.

The sawtooth is useful as a waveform, fm, and am

Usage: `wave.set`(Waveform.SAWTOOTH1, 1.0)`
            --- '''
            self.__function = self.__asd
            if    data and (isinstance(data, int) or isinstance(data, float)): data = [data, 0.0, 0.0]
            elif  data and  isinstance(data, list) and len(data) == 1: data = [0.0, 0.0, data[0]]
            else: data = [0.0, 0.0, 1.0]   # default to a full attack
            self.properties = data
            pass
        elif type == Waveform.SAWTOOTH2:
            ''' ---
##### Sawtooth2 Wave Function
A sawtooth wave is a special case of a ASD wave where only attack has a non-zero value.

The sawtooth is useful as a waveform, fm, and am

Usage: `wave.set`(Waveform.SAWTOOTH2, 1.0)`
            --- '''
            self.__function = self.__asd
            if    data and (isinstance(data, int) or isinstance(data, float)): data = [data, 0.0, 0.0]
            elif  data and  isinstance(data, list) and len(data) == 1: data = [data[0], 0.0, 0.0]
            else: data = [1.0, 0.0, 0.0]   # default to a full attack
            self.properties = data
            pass
        elif type == Waveform.TRIANGLE:
            ''' ---
##### Triangle Wave Function
A triangle wave is a special case of a ASD wave where attack and decay are of equal value
and sustain is 0.0.

The `ASD` type may be used to create a customer triangle where the attack and decay times are different.

The triangle is useful as a waveform, fm, and for am

Usage: `wave.set`(Waveform.TRIANGLE, None)`
            --- '''
            self.__function = self.__asd
            self.properties = [0.5, 0.0, 0.5]   # default to a uniform triangle
            pass
        elif type == Waveform.GAUSSIAN:
            ''' ---
##### Gaussian Wave Function
A gaussian wave resembles sharp peaks with gentle valleys.
The relationship of their sizes is controlled with `width`.

The gaussian is most useful as am

Usage: `wave.set`(Waveform.GAUSSIAN, 0.2)`
            --- '''
            self.__function = self.__gaussian
            if data and (isinstance(data, int) or isinstance(data, float)): data = [data, 0.0, 0.0]
            elif not data or not (isinstance(data, list) and (len(data) == 1)): data = [0.15, 0.0, 0.0]
            self.properties = data # default to moderate width
            pass
        elif type == Waveform.SINC:
            ''' ---
##### Sinc Wave Function
A sinc wave resembles ripples which increase in amplitude to a peak and diminish at the same rate.
The rate of the transition is controlled by the `width`.

The sinc is most useful as a waveform and for am

Usage: `wave.set`(Waveform.SINC, 0.015)`
            --- '''
            self.__function = self.__sinc
            self.amplitude = 0.5
            self.offset = 0.25
            if    data and (isinstance(data, int) or isinstance(data, float)): data = [data, 0.0, 0.0]
            elif  data and  isinstance(data, list) and len(data) == 1: data = [data[0], 0.0, 0.0]
            else: data = [0.015, 0.0, 0.0]   # default
            self.properties = data
            pass
        elif type == Waveform.EXPONENTIAL:
            ''' ---
##### Exponential Wave Function
An exponential wave is the standard exponent maths function.
The rate of the curve with `width`.

The exponential is most useful for am

Usage: `wave.set`(Waveform.EXPONENTIAL, 0.25)`
            --- '''
            self.__function = self.__exponential
            if    data and (isinstance(data, int) or isinstance(data, float)): data = [data, 0.0, 0.0]
            elif  data and  isinstance(data, list) and len(data) == 1: data = [data[0], 0.0, 0.0]
            else: data = [0.25, 0.0, 0.0] # default
            self.properties = data # default to moderate width
            pass
        elif type == Waveform.NOISE:
            ''' ---
##### Noise Wave Function
A noise wave is simply random data.
The variability is controlled by an interval value `quality`.
TBH: I can't tell the difference for any given quality but the range is presumable 1 to about 100.

The noise is most useful as a wave in conjunction with fm or am

Usage: `wave.set`(Waveform.NOISE, 10)`
            --- '''
            self.__function = self.__noise
            self.amplitude = 0.5
            self.offset = 0.25
            if    data and (isinstance(data, int) or isinstance(data, float)): data = [data, 0.0, 0.0]
            elif  data and  isinstance(data, list) and len(data) == 1: data = [data[0], 0.0, 0.0]
            else: data = [1.0, 0.0, 0.0]   # default
            self.properties = data # default to uniform
            pass
        else: # NONE
            self.__function = None
            pass


    def sample(self, sample):
        # evaluate the content of a wave at a sampling point
        if self.type == 0: return 0.0

        am, fm = 1.0, 0.0

        # get any modifiers for the waveform ... affecting amplitude or frequency (via phase)
        am = self.am.__function(sample) if self.am and (self.am.type > 0) else 1.0
        fm = self.fm.__function(sample) if self.fm and (self.fm.type > 0) else 0.0

        sample = (sample * self.multiplier) - self.phase + fm
        sample = sample - floor(sample) # quick way to clamp the value between 0.0 and 1.0

        val = self.__function(sample) if self.__function else 0.0

        val *= (self.amplitude * am)

        val += self.offset

        #print (val)
        return val

# end of Waveform class



''' ---
--------------------------------------------------------------------------
### DacChannel Class

The `DacChannel` class manages the construction, initialization, and operation of the PIO and its two DMA channels.
The PIO output is to 8 consecutive GPIO pins and assumes those pins are connected via an R-2R resistor ladder.

**NOTE:** the API supports up to 2 DacChannel objects - constructed with `(0)` and `(1)`

--- '''
class DacChannel:
    ''' ---
- a DacChannel manages the state machine and the two associated DMA channels
- a DacChannel contains the two sampling buffers
- a DacChannel contains a Waveform object (Waveform objects could be extended to contain other Waveform objects)
- a DacChannel contains the active frequency
- a DacChannel uses one of hte TX registers of PIO(0)
    --- '''

    ''' ---
#### DacChannel Constructor
The `DacChannel` constructor takes at least one parameter - either 0 or 1 to indicate which DAC channel is being created.

There are three optional parameters, with defaults:
- `size` - the buffer size to create for each fo the 2 DMA channels used (default = 1024)
- `pin` - the first GPIO pin of the R-2R resistor ladder used for the digital analog conversion (default = 0)
- `reversed` - if the R-2R pins are wired in reverse order (default = False)

_It can be helpful to wire the first R-2R normal and the second R-2R reversed so the output pins are physically adjacent._

Usage: `dac0 = DacChannel(0, pin=2) # create DAC 0 starting with GPIO2`

A complete use case looks something like this ...
    '''
    '''python

from awg2chan import Waveform, DacChannel

# wrap the code in a try-catch to allow for cleanup of the DMA and PIO
try:
    print("starting setup ...")
    dac0 = DacChannel(0, pin=2, size=512)
    dac0.wave.set(Waveform.SINE, None)
    dac0.start(5000)

    dac1 = DacChannel(1, pin=10, size=512, reversed=True)
    dac1.wave.set(Waveform.ASD, [0.20, 0.0, 0.80])
    dac1.start(2500)

    print("... setup complete")

    toggle = True
    while (1):
        freq = 5000 if toggle else 2500
        toggle = not toggle
        dac1.change_frequency(freq)
        sleep(0.5)

except Exception as e:
    print_exception(e)
    if dac0:
        dac0.exit_handler()
    if dac1:    
        dac1.exit_handler()

finally:
    if dac0:
        dac0.exit_handler()
        del dac0
    if dac1:
        dac1.exit_handler()
        del dac1

    '''
    '''
    --- '''
    def __init__ (self, dac, size=DEFAULT_SAMPLING_SIZE, pin=0, reversed=False, debug=0):
        if dac < 0 or dac > 1:
            print("ERROR: channel our of range - must be 0 or 1")

        self.debug = debug  # let the user have some control over debugging messages
        self.__dac_num = dac  # remember if we are working for DAC0 or DAC1
        self.__buffer_size = size
        self.__pin = pin
        self.__reverse = reversed
        self.__frequency = 0
        self.__frequency_actual = 0

        self.__sampling_buffer = {}
        self.__sampling_buffer[0] = bytearray(size)
        self.__sampling_buffer[1] = bytearray(size)
        self.__active_buffer = 0
        self.wave = Waveform()
        self.wave.fm = Waveform()
        self.wave.am = Waveform()

        ''' ---
The `wave` property is public and contains a `Waveform` object.
Use the `Waveform` class properties and methods to configure the `wave`.

Usage: `dac.wave.set(Waveform.SINC, 0.015)`
Usage: `dac.wave.amplitude = 0.5`
        --- '''

        self.__buffer_dereference = array('i', [0]) # integer array

        self.__dma_channel_address = {}
        self.__dma_channel_address[0] = DMA0_BASE + (((dac * 2) + 0) * DMA_CHANNEL_SIZE)
        self.__dma_channel_address[1] = DMA0_BASE + (((dac * 2) + 1) * DMA_CHANNEL_SIZE)
        if self.debug > 1:
            print("DMA Channel addresses: %X  %x" %(self.__dma_channel_address[0], self.__dma_channel_address[1]))

        # create the two DMA channel structs; aka self.__dma_channel[0] and self.__dma_channel[1]
        self.__dma_channel = {}
        # the 'struct' operation takes a memory address and maps a structure definition to it
        self.__dma_channel[0] = struct(self.__dma_channel_address[0], DMA_LAYOUT)
        self.__dma_channel[1] = struct(self.__dma_channel_address[1], DMA_LAYOUT)

        for i in range (0,2):
            # set channel to no wraparound(aka ring is 0), sniff=0, swap_byte=0, irq_quiet=1, no pacing of transfers (=0x3f). high prio=0, data_size=word, incr_r/w = true
            self.__dma_channel[i].CTRL_TRIG_RAW = 0x3f8030
            # set chain to itself, to disable chaining
            self.__dma_channel[i].CTRL_TRIG.CHAIN_TO = i

        '''
            the PIO_BASE is a property because, at one point, each DAC was on a separate PIO
            however, it was then discovered we only needed separate state machines for the PACs
            and could share a single PIO.
            When using a single PIO and separate state machines, we need to use different 
            TX addresses, TF wait flags, and different clock dividers.
        '''
        self.__pio = PIO0_BASE
        self.__pio_tx = PIO0_BASE + PIO0_TXF_OFFSET + (dac * PIO0_TXF_SIZE)
        self.__pio_tx_wait = dac  # the PIO has 0..3 TXF; which are we waiting on?
        self.__pio_clock_divider_address = PIO0_BASE + PIO0_CLKDIV_OFFSET + (dac * PIO0_CLKDIV_SIZE)
        self.__clock_divider = 0
        if self.debug > 1:
            print("PIO  addr:%X  tx:%X  wait:%d  clkaddr:%X" % (self.__pio, self.__pio_tx, self.__pio_tx_wait, self.__pio_clock_divider_address))

        self.__sm = StateMachine(dac, pio_stream_bits, PICO_CLOCK_SPEED, out_base=Pin(self.__pin))
        self.__sm.active(0)   # initial state machine is inactive

    ''' ---
#### dac Property (read-only)
The `DacChannel` DAC number, 0 or 1
    --- '''
    @property
    def dac(self):
        return self.__dac_num

    ''' ---
#### sampling Property (read-only)
The `DacChannel` the samples size aka the size of the sample buffer
    --- '''
    @property
    def sampling(self):
        return self.__buffer_size

    ''' ---
#### frequency Property (read-only)
The `DacChannel` current frequency
    --- '''
    @property
    def frequency(self):
        return self.__frequency

    ''' ---
#### active Property (read-only)
The `DacChannel` is running (or not) boolean
    --- '''
    @property
    def active(self):
        return (self.__frequency > 0)

    ''' ---
#### DacChannel Destructor
The `DacChannel` destructor insures the DMA channels are properly shutdown and the PIO state machine is stopped.
    --- '''
    def __del__(self):
        self.exit_handler()


    ''' ---
#### exit_handler() Method
The `exit_handler()` insures the DMA channels are properly shutdown and the PIO state machine is stopped.
It is called from the class destructor.

Usage: `dac.exit_handler()`
    --- '''
    def exit_handler(self):
        self.stop() # try to stop the DMA and PIO 'cleanly'

        # stop all DMA channels
        mem32[DMA_ABORT] = 0xFFFF
        while int(mem32[DMA_ABORT]) != 0:
            time.sleep_us(0.05)

        d0 = self.__dma_channel[0]
        d1 = self.__dma_channel[1]

        d0.AL1_CTRL = d0.AL1_CTRL & 0xFFFFFFFE
        d1.CTRL_TRIG_RAW = d1.CTRL_TRIG_RAW & 0xFFFFFFFE
        d1.CTRL_TRIG.EN = 0
        d0.CTRL_TRIG.EN = 0
        print ('DAC %d DMA channels stopped' % (self.__dac_num))

        self.__sm.active(0)
        print ('DAC %d state machine stopped' % (self.__dac_num))
        print('state machine for DAC %d stopped' % (self.__dac_num))

        rp2.PIO(0).remove_program()
        print('PIO cleared')

    # helper function to reverser the bits in a value
    # used when a DAC R-2R is wired right-to-left vs left-to-right
    def __reverse_bits(self, val):
        rtn = 0
        for i in range(DAC_SIZE):
            if (val & (1 << i) != 0):
                rtn += (1 << (DAC_SIZE - 1 - i))
        return rtn


    def __stop_dma(self):
        d0 = self.__dma_channel[0]
        d1 = self.__dma_channel[1]
        d1.CTRL_TRIG.EN = 0
        d0.CTRL_TRIG.EN = 0
        self.__sm.active(0)
        d1.AL1_CTRL = 0
        d0.AL1_CTRL = 0


    def __start_dma(self):
        transfers = int(self.__buffer_size_used / SAMPLE_PER_32BITS)

        if self.debug > 1:
            print("DMA Channel addresses: %X  %x" %(self.__dma_channel_address[0], self.__dma_channel_address[1]))

        d0 = self.__dma_channel[0]
        d1 = self.__dma_channel[1]

        buff = self.__sampling_buffer[self.__active_buffer]
        self.__buffer_dereference[0] = addressof(buff)

        # stop the state machine and DMA (they may not yet be active)
        d1.CTRL_TRIG.EN = 0
        d0.CTRL_TRIG.EN = 0
        self.__sm.active(0)
        d1.AL1_CTRL = 0
        d0.AL1_CTRL = 0

        # first DMA channel transfers data to PIO
        d0.READ_ADDR = addressof(buff)
        d0.TRANS_COUNT = transfers
        d0.WRITE_ADDR = self.__pio_tx

        d0.CTRL_TRIG.INCR_WRITE = 0
        d0.CTRL_TRIG.INCR_READ = 1
        d0.CTRL_TRIG.DATA_SIZE = DMA_XFER_SIZE

        d0.CTRL_TRIG.IRQ_QUIET = 1
        d0.CTRL_TRIG.TREQ_SEL = self.__pio_tx_wait
        d0.CTRL_TRIG.CHAIN_TO = (self.__dac_num * 2) + 1
        d0.CTRL_TRIG.RING_SEL = 0
        d0.CTRL_TRIG.RING_SIZE = 0
        d0.CTRL_TRIG.HIGH_PRIO = 1

        # second DMA channels transfers data to first DMA channel
        d1.READ_ADDR = addressof(self.__buffer_dereference)
        d1.TRANS_COUNT = 1
        d1.WRITE_ADDR = self.__dma_channel_address[0]

        d1.CTRL_TRIG.INCR_WRITE = 0
        d1.CTRL_TRIG.INCR_READ = 0
        d1.CTRL_TRIG.DATA_SIZE = 2 # 0=8bit, 1=16bit, 2=32bit

        d1.CTRL_TRIG.IRQ_QUIET = 1
        d1.CTRL_TRIG.TREQ_SEL = DMA_NO_PACING
        d1.CTRL_TRIG.CHAIN_TO = (self.__dac_num * 2)
        d1.CTRL_TRIG.RING_SEL = 0
        d1.CTRL_TRIG.RING_SIZE = 0
        d1.CTRL_TRIG.HIGH_PRIO = 1

        # start the state machine and then the DM channels
        self.__sm.active(1)
        d1.CTRL_TRIG.EN = 1
        d0.CTRL_TRIG.EN = 1


    ''' ---
#### start() Method
The `start()` method takes one parameter:
- `frequency` - the frequency to use when generated the set waveform (waveform must be set prior to using `start()`)

Usage: `dac.start(440)`
    --- '''
    def start(self, freq):
        if not freq:
            print("error: frequency must be greater than 0")
            return
        
        self.__frequency = freq
        self.wave.__shift = 0.0

        self.__active_buffer = (self.__active_buffer + 1) % 2   # use 'other' buffer
        active = self.__active_buffer

        max_sample_size = self.__buffer_size

        # calculate the clock divider 
        computed_div = PICO_CLOCK_SPEED / (freq * max_sample_size) # required clock divider for maximum buffer size

        '''
        # our generated frequency is more accurate when the clock_div is higher
        # we could by reducing the buffer_size
        while (computed_div < 256.0) and (max_sample_size > 64):
            # half the available buffer
            max_sample_size /= 2
            computed_div = PICO_CLOCK_SPEED / (freq * max_sample_size) # required clock divider for maximum buffer size
            print("divider: %f  samples: %d" % (computed_div, max_sample_size))
        '''

        if computed_div < CLOCK_THRESHOLD:  # too fast so we will repeat the waveform within the period
            repeat = int(CLOCK_THRESHOLD / computed_div)
            sample_size = (max_sample_size * computed_div * repeat)
            clock_div = int(CLOCK_THRESHOLD)  # fastest we can make the clock go
        else:        # stick with integer clock division only
            clock_div = int(computed_div + CLOCK_THRESHOLD + 0.5)
            sample_size = max_sample_size * computed_div / clock_div
            repeat = 1

        sample_size = int((sample_size + 0.5) / SAMPLE_PER_32BITS) * SAMPLE_PER_32BITS

        # fill the buffer
        for i in range(sample_size):
            val = 0.0
            val = self.wave.sample((repeat * (i + 0.5) / sample_size))
            val = int(((1<<DAC_SIZE)-1) * val)  # FYI: '((1<<DAC_SIZE)-1)' is 255 for an 8bit DAC
            val = clamp(val, 0, ((1<<DAC_SIZE)-1))

            if self.__reverse:
                val = self.__reverse_bits(val)
            self.__sampling_buffer[active][i] = val
            if self.debug > 2:
                if i % 24 == 0:
                    print("")
                print("%4d " % (self.__sampling_buffer[active][i]), end="")

        print("what: ", sample_size, max_sample_size)
        for i in range(sample_size, max_sample_size):
            self.__sampling_buffer[active][i] = 0

        if self.debug > 2:
            print("")

        self.__buffer_size_used = sample_size

        # recompute on the actual number of samples
        re_computed_div = (PICO_CLOCK_SPEED / (freq * self.__buffer_size_used))
        if clock_div > 1:
            clock_div = (int(re_computed_div - 0.5)) + 1

        self.__frequency_actual = (PICO_CLOCK_SPEED / clock_div / self.__buffer_size_used) * repeat
        if self.debug > 0:
            print("freq: %6d actual: %8.2f   using %4d of %4d samples  repeat:%d  clockdiv:%4d (%8.4f) err: %+5.4f" % (self.__frequency, self.__frequency_actual, self.__buffer_size_used, self.__buffer_size, repeat, clock_div, re_computed_div, (re_computed_div - clock_div)))
            #print("freq: %6d actual: %8.2f" % (self.__frequency, self.__frequency_actual))
            #print("using %4d of %4d samples" % (self.__buffer_size_used, self.__buffer_size))
            #print("repeat:%d  clockdiv:%4d" % (repeat, clock_div))
            #print("err: %+5.4f" % ((re_computed_div - clock_div)))

        # set the clock divider
        self.__clock_divider= min(clock_div, 65535) 
        mem32[self.__pio_clock_divider_address] = ((self.__clock_divider << 16) | (0 << 8))  # no fractional clock divider
        self.__sm.active(0)
        self.__start_dma()
        self.__sm.active(1)


    ''' ---
#### stop() Method
The `stop()` method takes no parameter:

Usage: `dac.stop()`
    --- '''
    def stop(self):
        if not self.__frequency: return

        # erase the sampling buffers
        for i in range(self.__buffer_size):
            self.__sampling_buffer[0][i] = 0
            self.__sampling_buffer[1][i] = 0
        # we wait a moment so the zero data propagates
        sleep(0.20) # good enough for anything above 7Hz
        # stop the DMA and state machine
        self.__stop_dma
        self.__frequency = 0
        self.wave.__shift = 0.0

    ''' ---
#### change_frequency() Method
The `change_frequency()` method takes one parameter:
- `frequency` - the new frequency to use with the currently started waveform.

Usage: `dac.change_frequency(880)`

_Changing the frequency by directly changing the clock divider works under most use cases
*BUT* for extreme changes or near an untested and undetermined boundary condition,
it will likely break. ... but it worked during testing. YMMV._
    --- '''
    def change_frequency(self, freq):
        # force a FULL_FREQUENCY_RECOMPUTE rather than attempt a quick clock divider change
        self.start(freq)	# this does a complete recompute
        '''
        # attempt a quick recalculation of the frequency; not as accurate but better at low frequencies
        self.__frequency = freq

        max_sample_size = self.__buffer_size
        threshold = 3.0   # TODO this is dependent on SAMPLE_PER_32BITS 1,2,3,4 => threshold 3.0,2.0,1.0,1.0

        # calculate the clock divider 
        computed_div = PICO_CLOCK_SPEED / (freq * max_sample_size * SAMPLE_PER_32BITS) # required clock divider for maximum buffer size

        if computed_div < threshold:  # too fast so we will repeat the waveform within the period
            repeat = int(threshold / computed_div)
            sample_size = (max_sample_size * computed_div * repeat) / threshold
            clock_div = int(threshold)  # fastest we can make the clock go
        else:        # stick with integer clock division only
            clock_div = int(computed_div + threshold + 0.5)
            sample_size = max_sample_size * computed_div / clock_div
            repeat = 1

        # recompute on the actual number of samples
        if clock_div > 1:
            re_computed_div = (PICO_CLOCK_SPEED / (freq * self.__buffer_size_used))
        clock_div = (int(re_computed_div - 0.5)) + 1

        self.__frequency_actual = int(PICO_CLOCK_SPEED / clock_div / self.__buffer_size_used) * repeat
        print("freq: %6d actual: %6d samples (%4d of %4d) with repeat of %d and clockdiv of %4d %8.4f  err: %+5.4f" % (self.__frequency, self.__frequency_actual, self.__buffer_size_used, self.__buffer_size, repeat, clock_div, re_computed_div, (re_computed_div - clock_div)))

        self.__clock_divider = min(clock_div, 65535) 
        # set the clock divider
        mem32[self.__pio_clock_divider_address] = ((self.__clock_divider << 16) | (0 << 8))  # no fractional clock divider
        '''

    ''' ---
#### shift_phase() Method
The `shift_phase()` method takes one parameter:
- `offset` - the amount +/- to shift the waveform (sift data within the buffer)

Usage: `dac.shift_phase(-0.1) # shift left 10%`

_Changing the waveform buffer data directly will work under most use cases
*BUT* there most likely will be a momentary glitch when the DMA is reading the buffer. YMMV._
    --- '''
    def shift_phase(self, shift):
        used = self.__buffer_size_used
        offset = int(used * (shift - self.wave.__shift))         # amount to shift the sampling buffer
        if self.debug > 1:
            print("phase:%3.2f  shift:%3.2f offset:%d" % (self.wave.__shift, shift, offset))

        self.wave.__shift = shift
        if (offset == 0): return

        buff = self.__sampling_buffer[self.__active_buffer]
        temp = self.__sampling_buffer[(self.__active_buffer + 1) % 2]

        '''
            I am sure there is a more efficient solution for rotating the data within a bytearray
            but for now, we copy off the offset bytes, move the remaining data by offset, and the copy back the offset bytes
        '''

        if offset < 0:
            offset = -offset
            if (offset >= used): return

            # direction is left
            # save the first 'offset' bytes
            # shift buffer left by 'offset'
            # copy 'offset' bytes to end of buffer
            ''' slow code
            for i in range(offset):                 temp[i] = buff[i]
            for i in range(used - offset):          buff[i] = buff[i + offset]
            for i in range(offset):                 buff[(used - offset) + i] = temp[i]
            '''
            ''' fast code '''
            buff_p = memoryview(buff)
            temp_p = memoryview(temp)
            temp_p[:offset] = buff_p[:offset]
            buff_p[:used-offset] = buff_p[offset:used]
            buff_p[used-offset:used] = temp_p[:offset]
        else:
            if (offset >= used): return

            # direction is right
            # save the last 'offset' bytes
            # shift buffer left by 'offset'
            # copy 'offset' bytes to start of buffer
            ''' slow code
            for i in range(offset):                 temp[i] = buff[(used - offset) + i]
            for i in range(used - offset):          buff[used - i - 1] = buff[(used - offset) - i]
            for i in range(offset):                 buff[i] = temp[i]
            '''
            ''' fast code '''
            buff_p = memoryview(buff)
            temp_p = memoryview(temp)
            temp_p[:offset] = buff_p[used - offset:used]
            buff_p[offset:used] = buff_p[:used-offset]
            buff_p[:offset] = temp_p[:offset]


# end of DacChannel class


'''
supervisor.ticks_ms() is a counter which wraps
to accommodate for the wrapping, we provide the following class

Code derived from:
https://docs.circuitpython.org/en/latest/shared-bindings/supervisor/
'''
class SimpleTimer:
    _TICKS_PERIOD = const(1<<29)
    _TICKS_MAX = const(_TICKS_PERIOD-1)
    _TICKS_HALFPERIOD = const(_TICKS_PERIOD//2)

    def __init__(self, interval=100):
        self._interval = interval
        self._milliseconds = 0
        self._running = False

    @property
    def interval(self):
        return self._interval
    @interval.setter
    def interval(self, interval):
        self._interval = interval

    def start(self, interval=None):
        if interval is None:
            interval = self._interval
        self._milliseconds = ticks_ms() + self._interval
        self._running = True
    def stop(self):
        self._milliseconds = 0
        self._running = False
    @property
    def expired(self):
        if self._running == False:
            return False
        # "Compute the signed difference between two ticks values, assuming that they are within 2**28 ticks"
        now = ticks_ms()
        diff = (self._milliseconds - now) & _TICKS_MAX
        diff = ((diff + _TICKS_HALFPERIOD) & _TICKS_MAX) - _TICKS_HALFPERIOD
        if diff < 0:
            self.stop()
            return True
        return False


# -------------------------------------------------------------------------------



''' ---
--------------------------------------------------------------------------
## EXAMPLES

There are several examples which demonstrate the use of `awg2chan`.
These provide a quick way to test the hardware and introduce the use of the API.
For a thorough understanding of what is possible with `awg2chan`, please read the API documentation (above).

--- '''
