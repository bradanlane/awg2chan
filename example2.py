''' ************************************************************************************
* File:    awg2chan example2.py
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
*
* Bradán Lane - 2023.10.24
*
* ************************************************************************************ '''


''' ---
### Example 1

This example creates a 2 sine waves of the same frequency
and then dynamically changes the phase of one +/- 100%.

This example 

The purpose of this example is to both demonstrate basic usage of the
`awg2chan` API and configuration parameters.

**NOTE:** this example was tested using hardware where 
the first R2R DAC starts at pin=2, the second DAC starts at pin=10,
and the second DAC is wired in reverse order.

--- '''

from time import sleep
from awg2chan import Waveform, DacChannel

try:
    print("starting setup ...")
    dac0 = DacChannel(0, pin=2, size=1024)
    dac0.wave.set(Waveform.SINE, None)
    dac0.start(5000)
    dac1 = DacChannel(1, pin=10, size=1024, reversed=True)
    dac1.wave.set(Waveform.SINE, None)
    dac1.start(5000)
    print("... setup complete")

    offset = 0.01
    phase = 0.0
    while (1):
        phase += offset
        if phase > 1.0:
            offset = -0.01
            phase = 1.0
            #print("phase shift left")
        elif phase < -1.0:
            offset = 0.01
            phase = -1.0
            #print("phase shift right")
        dac1.shift_phase(phase)
        sleep(0.1)

except Exception as e:
    print("Exception:")
    print_exception(e)
    if dac0:
        dac0.exit_handler()
    if dac1:    
        dac1.exit_handler()

finally:
    print("Final:")
    if dac0:
        dac0.exit_handler()
        del dac0
    if dac1:
        dac1.exit_handler()
        del dac1
