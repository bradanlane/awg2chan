
# AWG 2 Channel Arbitrary Waveform Generator

An 'arbitrary waveform generator' or AWG is able to generate a range of waveforms.
This code uses the Raspberry Pi PICO and its PIO, state machines, and DMA to
provide two completely independent waveforms.

Each waveform generates values between 0v and VCC (3.3V on the PICO).
It is possible to add a combination of voltage dividers and an opamp
to provide larger or smaller ranges or make the output a +/- range.

**IMPORTANT:** This is for educational purposes only.
The accuracy of the generated frequencies may not be sufficient.

Testing results: 
- sampling buffer size = 4096 has an error of +/- 0.50 (with a max error of 0.75)
- sampling buffer size = 8192 has an error of +/- 0.25 (with a max error of 0.50)

Of course, you are welcome to try and improve it.
If you find improvements, you are encouraged to submit a pull-request.
Thank you.

--------------------------------------------------------------------------


--------------------------------------------------------------------------
## Functionality

The 'arbitrary waveform generator' or AWG is able to generate a range of waveforms.
Each waveform has zero or more properties.
The waveform is independent of the frequency.
This allows for the waveform to be created and reused at difference frequencies.

--------------------------------------------------------------------------
## Hardware

The `awg2chan.py` code has been written for and tested with the Raspberry Pi PICO.
It is possible to use other PR2040 boards provided enough sequential GPIO exposed.
The board must have two sets of 8 sequential GPIO pins suitable for output.
Each sequence of pins becomes an 8-bit DAC through the creation of an R-2R ladder.

> An R–2R ladder configuration is a simple and inexpensive way to perform digital-to-analog conversion (DAC),
> using repetitive arrangements of precise resistor networks in a ladder-like configuration.

The most compact R-2R for the PICO uses 0603 SMD resistors which are soldered directly onto the board.
The test hardware uses 1KOhm 1% 0603 resistors.

<div align="center">![PICO with R-2R DACs](files/awg2chan_large.png){width=75%}<br/>
click for larger image<br/><br/></div>

## Software

The `awg2chan.py` is written in MicroPython.
The Raspberry Pi PICO requires the MicroPython [UF2](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html#drag-and-drop-micropython) installed.

If you wish to have `awg2chan.py` run automatically every time the PICO is powered, it needs to be installed onto the PICO.
Copy `awg2chan.py` to the PICO (most people use Thonny or ampy) and name the file `main.py`.

### Architecture

There are multiple steps to go from waveform to DAC output:

- the mathematical waveform is sliced into 'n' sample values _(where 'n' is a defined constant for each channel) and stored in a buffer
- chained DMA channels are initialized for the buffer
  - channel 1 chunks the sample buffer into 32 bit blocks and loads one block at a time to channel 0
  - channel 0 loads 8 bits at a time into the PIO FIFO
  - this process repeats indefinitely
- the PIO takes 8 bits at a time and set the 8 consecutive GPIO pins high or low according the bits as 0 or 1
- the R-2R latter combines the GPIO pins hi/lo into an analog voltage

At any time, a new waveform may be sliced into a sample buffer.
To reduce the delay, there are two samples buffers per channel.

There are 2 independent channels which results in a total of 4 sampling buffers.

The sampling size effects the quality of the analog waveform and is dependent of the usable frequency range.
A larger sampling buffer is needed to form reasonable waveforms at higher frequencies,
while a smaller sampling buffer is useable for waveforms in the audio range.

--------------------------------------------------------------------------
## API

The API is comprised of 2 classes `Waveform` and `DacChannel`.

The `DacChannel` is the primary class and controls the operations of the two available DACs.
A `DacChannel` object has a `Waveform` which controls the generator function for the output signal.

**NOTE:** a future enhancement is to allow _modifiers_ for waveforms - i.e. mathematically combining waveforms.


--------------------------------------------------------------------------
### Waveform Class

The `Waveform` class manages the construction, initialization, and slicing of a waveform.

There are 9 available waveforms:
1. [SINE](https://en.wikipedia.org/wiki/Sine_wave)
1. [ASD] (generic attack-sustain-decay)
1. [SQUARE](https://en.wikipedia.org/wiki/Square_wave) (ASD with zero attack and zero decay)
1. [SAWTOOTH1](https://en.wikipedia.org/wiki/Sawtooth_wave) (ASD with zero attack and zero sustain)
1. [SAWTOOTH2](https://en.wikipedia.org/wiki/Sawtooth_wave) (ASD with zero sustain and zero decay)
1. [TRIANGLE](https://en.wikipedia.org/wiki/Triangle_wave) (ASD with zero sustain and uniform attack and decay)
1. [SINC](https://en.wikipedia.org/wiki/Sinc_function)
1. [GAUSSIAN](https://en.wikipedia.org/wiki/Gaussian_function)
1. [EXPONENTIAL](https://en.wikipedia.org/wiki/Exponential_function)
1. [NOISE](https://en.wikipedia.org/wiki/Perlin_noise)


#### Waveform Constructor
The `Waveform` constructor takes no parameters.
It initializes the public and private properties.
The default wave function is set to `None`.

Usage: `my_wave = Waveform()`

Each waveform has four public properties:
- amplitude - initial waveform amplitude
- phase - initial phase of the waveform
- offset - initial voltage shift of the waveform
- multiplier - a horizontal multiplier (used for fm and am; must be an integer value)
- fm - optional second waveform to modulate the frequency of the base waveform
- am - optional waveform to modulate teh amplitude of the base waveform

Usage: `wave.amplitude = 0.7071`
Usage: `wave1.envelop = wave2`
    
#### set() Method
The `wave.set()` method takes two parameters:
- the type constants (e.g. `Waveform.PULSE`)
- an optional `data=value` or `data=list` parameter (lists must contain the required number of values for the waveform)
    
##### Sine Wave Function
A sine wave function. Has no additional properties

The sine wave is most useful as a waveform and can be used as fm or am

Usage: `wave.set`(Waveform.SINE, None)`
            
##### ASD Wave Function
An ASD wave is a generic angular waveform with three possible stages:
- attack - the fractional time spent transitioning for 0.0 to `amplitude`
- sustain - the fractional time spent at `amplitude`
- decay - the fractional time spent transitioning from `amplitude` to 0.0

The ASD is most useful for am

Usage: `wave.set`(Waveform.PULSE, [0.25, 0.25, 0.5])`
            
##### Square Wave Function
A square wave is a special case of a ASD wave where both attack and decay are 0.0
and the sustain value defines the duty cycle of the square wave.

The square is useful as a waveform, am, and fm

Usage: `wave.set`(Waveform.SQUARE, 0.66)`
            
##### Sawtooth1 Wave Function
A sawtooth wave is a special case of a ASD wave where only decay has a non-zero value.

The sawtooth is useful as a waveform, fm, and am

Usage: `wave.set`(Waveform.SAWTOOTH1, 1.0)`
            
##### Sawtooth2 Wave Function
A sawtooth wave is a special case of a ASD wave where only attack has a non-zero value.

The sawtooth is useful as a waveform, fm, and am

Usage: `wave.set`(Waveform.SAWTOOTH2, 1.0)`
            
##### Triangle Wave Function
A triangle wave is a special case of a ASD wave where attack and decay are of equal value
and sustain is 0.0.

The `ASD` type may be used to create a customer triangle where the attack and decay times are different.

The triangle is useful as a waveform, fm, and for am

Usage: `wave.set`(Waveform.TRIANGLE, None)`
            
##### Gaussian Wave Function
A gaussian wave resembles sharp peaks with gentle valleys.
The relationship of their sizes is controlled with `width`.

The gaussian is most useful as am

Usage: `wave.set`(Waveform.GAUSSIAN, 0.2)`
            
##### Sinc Wave Function
A sinc wave resembles ripples which increase in amplitude to a peak and diminish at the same rate.
The rate of the transition is controlled by the `width`.

The sinc is most useful as a waveform and for am

Usage: `wave.set`(Waveform.SINC, 0.015)`
            
##### Exponential Wave Function
An exponential wave is the standard exponent maths function.
The rate of the curve with `width`.

The exponential is most useful for am

Usage: `wave.set`(Waveform.EXPONENTIAL, 0.25)`
            
##### Noise Wave Function
A noise wave is simply random data.
The variability is controlled by an interval value `quality`.
TBH: I can't tell the difference for any given quality but the range is presumable 1 to about 100.

The noise is most useful as a wave in conjunction with fm or am

Usage: `wave.set`(Waveform.NOISE, 10)`
            
--------------------------------------------------------------------------
### DacChannel Class

The `DacChannel` class manages the construction, initialization, and operation of the PIO and its two DMA channels.
The PIO output is to 8 consecutive GPIO pins and assumes those pins are connected via an R-2R resistor ladder.

**NOTE:** the API supports up to 2 DacChannel objects - constructed with `(0)` and `(1)`


- a DacChannel manages the state machine and the two associated DMA channels
- a DacChannel contains the two sampling buffers
- a DacChannel contains a Waveform object (Waveform objects could be extended to contain other Waveform objects)
- a DacChannel contains the active frequency
- a DacChannel uses one of hte TX registers of PIO(0)
    
#### DacChannel Constructor
The `DacChannel` constructor takes at least one parameter - either 0 or 1 to indicate which DAC channel is being created.

There are three optional parameters, with defaults:
- `size` - the buffer size to create for each fo the 2 DMA channels used (default = 1024)
- `pin` - the first GPIO pin of the R-2R resistor ladder used for the digital analog conversion (default = 0)
- `reversed` - if the R-2R pins are wired in reverse order (default = False)

_It can be helpful to wire the first R-2R normal and the second R-2R reversed so the output pins are physically adjacent._

Usage: `dac0 = DacChannel(0, pin=2) # create DAC 0 starting with GPIO2`

A complete use case looks something like this ...
    '''
    '''python

from awg2chan import Waveform, DacChannel

# wrap the code in a try-catch to allow for cleanup of the DMA and PIO
try:
    print("starting setup ...")
    dac0 = DacChannel(0, pin=2, size=512)
    dac0.wave.set(Waveform.SINE, None)
    dac0.start(5000)

    dac1 = DacChannel(1, pin=10, size=512, reversed=True)
    dac1.wave.set(Waveform.ASD, [0.20, 0.0, 0.80])
    dac1.start(2500)

    print("... setup complete")

    toggle = True
    while (1):
        freq = 5000 if toggle else 2500
        toggle = not toggle
        dac1.change_frequency(freq)
        sleep(0.5)

except Exception as e:
    print_exception(e)
    if dac0:
        dac0.exit_handler()
    if dac1:    
        dac1.exit_handler()

finally:
    if dac0:
        dac0.exit_handler()
        del dac0
    if dac1:
        dac1.exit_handler()
        del dac1

    '''
    '''
    
The `wave` property is public and contains a `Waveform` object.
Use the `Waveform` class properties and methods to configure the `wave`.

Usage: `dac.wave.set(Waveform.SINC, 0.015)`
Usage: `dac.wave.amplitude = 0.5`
        
#### dac Property (read-only)
The `DacChannel` DAC number, 0 or 1
    
#### sampling Property (read-only)
The `DacChannel` the samples size aka the size of the sample buffer
    
#### frequency Property (read-only)
The `DacChannel` current frequency
    
#### active Property (read-only)
The `DacChannel` is running (or not) boolean
    
#### DacChannel Destructor
The `DacChannel` destructor insures the DMA channels are properly shutdown and the PIO state machine is stopped.
    
#### exit_handler() Method
The `exit_handler()` insures the DMA channels are properly shutdown and the PIO state machine is stopped.
It is called from the class destructor.

Usage: `dac.exit_handler()`
    
#### start() Method
The `start()` method takes one parameter:
- `frequency` - the frequency to use when generated the set waveform (waveform must be set prior to using `start()`)

Usage: `dac.start(440)`
    
#### stop() Method
The `stop()` method takes no parameter:

Usage: `dac.stop()`
    
#### change_frequency() Method
The `change_frequency()` method takes one parameter:
- `frequency` - the new frequency to use with the currently started waveform.

Usage: `dac.change_frequency(880)`

_Changing the frequency by directly changing the clock divider works under most use cases
*BUT* for extreme changes or near an untested and undetermined boundary condition,
it will likely break. ... but it worked during testing. YMMV._
    
#### shift_phase() Method
The `shift_phase()` method takes one parameter:
- `offset` - the amount +/- to shift the waveform (sift data within the buffer)

Usage: `dac.shift_phase(-0.1) # shift left 10%`

_Changing the waveform buffer data directly will work under most use cases
*BUT* there most likely will be a momentary glitch when the DMA is reading the buffer. YMMV._
    
--------------------------------------------------------------------------
## EXAMPLES

There are several examples which demonstrate the use of `awg2chan`.
These provide a quick way to test the hardware and introduce the use of the API.
For a thorough understanding of what is possible with `awg2chan`, please read the API documentation (above).


### Example 1

This example creates a single channel sine wave and changes it frequency through 
a range. The purpose of this example is to both demonstrate basic usage of the
`awg2chan` API and to check the accuracy of the generated sine wave.

**NOTE:** this example was tested using hardware where the first R2R DAC starts at pin=2


### Example 3

This example creates a USB serial interface and command set
for dynamically interacting with the `awg2chan` API.

**IMPORTANT:** this example uses the `stringscmd` library.
_see below_

**NOTE:** this example was tested using hardware where 
the first R2R DAC starts at pin=2, the second DAC starts at pin=10,
and the second DAC is wired in reverse order.


--------------------------------------------------------------------------
## StringCommand

A string interface to the `awg2chan` arbitrary waveform generator.
This library is typically used in conjunction with a USB Serial read/write
service.

The library is used by one of the examples for `awg2chan`.


--------------------------------------------------------------------------
### StringCommand Class

The `StringCommand` class provides a command string interface for `awg2chan`.

It was created as a demonstration for interactive with `awg2chan` using a USB serial connection.



--------------------------------------------------------------------------
#### StringCommand Constructor
The constructor for the `StringCommand` class takes no arguments
    
#### StringCommand Destructor
The destructor for `StringCommand` insures the DacChannel objects have an opportunity to stop and perform any necessary cleanup.
    
#### process() Method
The `process()` method takes one parameter
- `command_string` - a string formatted according to the supported commands detailed in the `help` text.
    
