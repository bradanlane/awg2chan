''' ************************************************************************************
* File:    I2C UI Hardware Widgets (widgetui.py)
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
*
* Bradán Lane - 2023.10.24
*
* ************************************************************************************ '''


''' ---
--------------------------------------------------------------------------
### Widget UI

This example uses a series of I2C UI Hardware devices - aka UI Widgets
to control `awg2chan`.

The UI consists of 2 displays - one per channel - and
2 banks - one per channel - of five rotary encoders each.

**NOTE:** this example was tested using hardware where 
the first R2R DAC starts at pin=2, the second DAC starts at pin=10,
and the second DAC is wired in reverse order.

--- '''

# using USB Serial to command awg2chan
# using I2C UI hardware to  command awg2chan

from utime import sleep

from awg2chan import Waveform, DacChannel, clamp

import select
import sys
from machine import I2C, Pin
from sys import print_exception

'''
-----------------------------------------------------------------------------------------------------
start of the i2c hardware user interface
-----------------------------------------------------------------------------------------------------
'''
GRADIENT_SIZE = 20
ui_gradient = [[0x00, 0x00, 0xFF],[0x00, 0x35, 0xFF],[0x00, 0x6B, 0xFF],[0x00, 0xA1, 0xFF],[0x00, 0xD6, 0xFF],[0x00, 0xFF, 0xF1],[0x00, 0xFF, 0xBB],[0x00, 0xFF, 0x86],[0x00, 0xFF, 0x50],[0x00, 0xFF, 0x1A],[0x1A, 0xFF, 0x00],[0x50, 0xFF, 0x00],[0x86, 0xFF, 0x00],[0xBB, 0xFF, 0x00],[0xF1, 0xFF, 0x00],[0xFF, 0xD6, 0x00],[0xFF, 0xA1, 0x00],[0xFF, 0x6B, 0x00],[0xFF, 0x35, 0x00],[0xFF, 0x00, 0x00]]


class Widget:

    ENCODER_ADDRESS  = 0x20
    DISPLAY_ADDRESS  = 0x24

    # the DEVICE_I2CC_* commands are common to all I2C Client Devices in the AVR Coding 102 materials
    DEVICE_I2CC_STATUS      = 0x01	# return any status code; resets any error
    DEVICE_I2CC_BRIGHTNESS  = 0x08	# 1 byte data; set the brightness scalar for color (0..255)
    DEVICE_I2CC_COLOR_BG    = 0x09	# 3 bytes data; set the color of the LED bars; all 0 values means gradient
    DEVICE_I2CC_COLOR_FG    = 0x0A	# 3 bytes data; set the color of the LED bars; all 0 values means gradient
    ENCODER_CMD_RANGE       = 0x11	# 2 byte data; change the upper limit for the specified encoder (default is 255)
    ENCODER_CMD_LED         = 0x12	# 7 bytes data; LED number and both default and active color assignments
    ENCODER_CMD_BUTTON      = 0x21	# return  2 bytes, representing a button (1..1) (0 means no key) and a 1 or 0 (pressed or released)
    ENCODER_CMD_VALUE       = 0x22  # takes 1 byte for the encoder number; returns 1 byte, the encoder value in the range 0..255
    ENCODER_CMD_DELTA       = 0x23  # takes 1 byte for the encoder number; returns 1 byte, the change in the encoder value since the last request; in the range -127..127
    DISPLAY_CMD_MODE        = 0x11	# 1 byte; one of the predefined modes
    DISPLAY_CMD_TEXT        = 0x12	# 2+N bytes; line number (0=append), size of text, text
    DISPLAY_CMD_TITLE       = 0x20	# 1+N bytes; size of title, title text
    DISPLAY_CMD_MENU_SETUP  = 0x21	# 2 bytes = number of menu items, highlighted item
    DISPLAY_CMD_MENU_ITEM   = 0x22	# 2+N bytes; item number, size of title, title text
    DISPLAY_CMD_GRAPH_SETUP = 0x24	# 2 bytes = total bars, size of series
    DISPLAY_CMD_GRAPH_VALUE = 0x25	# 2 bytes, a bar (1..20), and a value
    DISPLAY_CMD_BUTTON      = 0x31	# return 2 bytes, representing a button (1..3) (0 means no key) and a 1 or 0 (pressed or released)
    DISPLAY_CMD_MENU_VALUE  = 0x32	# return 1 byte with the current highlighted menu item
    DISPLAY_MODE_TEXT_SMALL = 0x11
    DISPLAY_MODE_TEXT_LARGE = 0x21
    DISPLAY_MODE_MENU_SMALL = 0x12
    DISPLAY_MODE_MENU_LARGE = 0x22
    DISPLAY_MODE_MESSAGE_SMALL = 0x13
    DISPLAY_MODE_MESSAGE_LARGE = 0x23
    DISPLAY_MODE_GRAPH_BAR  = 0x04
    DISPLAY_BUTTON_UP       = 1
    DISPLAY_BUTTON_SELECT   = 2
    DISPLAY_BUTTON_DOWN     = 3

    # UNITS = [0, 0, 0, 'Hz', '%', ' ']    # used as teh trailing units indicator on the display
    WAVEFORM_NAMES = ["none", "sine", "asd", "square", "sawtooth1", "sawtooth2", "triangle", "gaussian", "sinc", "exponential", "noise"]
    DISPLAY_LABELS = ['Waveform', 'FM', 'AM', 'Frequency', 'Phase', 'Status', 'Amplitude', 'Offset', 'Param 1', 'Param 2', 'Param 3']



    def __init__(self, dac, i2c=None):
        self.dac_num = dac
        self.dac = None
        self.i2c = i2c
        self.encoders_last = [0,0,0,0,0]
        self.buttons = [0,0,0,0,0]   # press mean advance the mode for the related encoder

        # assumes the I2C addresses start at their base and are sequential
        self.display_i2c = Widget.DISPLAY_ADDRESS + self.dac_num
        self.encoder_i2c = Widget.ENCODER_ADDRESS + self.dac_num

        if not i2c:
            print("scanning for UI hardware")
            # should not be necessary, but perhaps the external pullups are missing?
            sda_pin = Pin(20, mode=Pin.OUT, pull=Pin.PULL_UP)
            scl_pin = Pin(21, mode=Pin.OUT, pull=Pin.PULL_UP)
            i2c = I2C(0, scl=scl_pin, sda=sda_pin, freq=100000) #, timeout=5000)
            print("pause to let UI hardware initialize (when first plugged in) ... ")
            sleep(6) # need to give hardware time to get online
            if not i2c:
                print("ERROR: unable to obtain I2C interface")
                return
        self.i2c = i2c
        address_list = self.i2c.scan()
        print ("I2C address list: ", address_list)
 
        # ---- find the UI hardware -----------------------------------------------
        try:
            address_list.index(self.encoder_i2c)
            print("We have the channel encoder device 0x%2X" % (self.encoder_i2c))
            address_list.index(self.display_i2c)
            print("We have the channel display device 0x%2X" % (self.display_i2c))
        except ValueError:
            print("ERROR: UI hardware not detected. UI support disabled")
            self.i2c = None
            return

        # ---- pre-configure the UI hardware --------------------------------------

        # configure encoder
        # encoders are waveform, multiplier, frequency, phase, ????
        self.i2c.writeto(self.encoder_i2c, bytes([Widget.DEVICE_I2CC_COLOR_BG, 0, 0, 0]))   # default to gradient
        self.i2c.writeto(self.encoder_i2c, bytes([Widget.DEVICE_I2CC_COLOR_FG, 255, 255, 255]))   # bright purple
        self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_RANGE, 1, 10]))   # waveform names
        self.i2c.writeto(self.display_i2c, bytes([Widget.DISPLAY_CMD_MODE, Widget.DISPLAY_MODE_TEXT_SMALL]))


        # initialize dac
        self.frequency = 0
        self.phase = 0.0
        self.dirty = False

        self.dac_size = 4096

        # these are specific to the test hardware; the UI does not expose the needed parameters for dac.create()
        if dac == 0:
            self.pin = 2
            self.reversed = False
        else:
            self.pin = 10
            self.reversed = True

        self.dac = DacChannel(self.dac_num, self.dac_size, pin=self.pin, reversed=self.reversed, debug=1)

        self.update_colors()  # colors for default mode
        self.refresh_display()


    # end __init__ ----------------------------------------------------



    def __del__(self):
        self.exit_handler()
        if self.i2c:
            del ui_i2c
            self.i2c = None

    def exit_handler(self):
            if self.dac:
                self.dac.exit_handler()

    
    # end __del__ -----------------------------------------------------



    def refresh_display(self, extra=' '):
        title = 'Channel ' + chr(0x30 + self.dac_num) + ' ' + str(extra)
        payload = bytearray()
        payload.append(Widget.DISPLAY_CMD_TITLE)
        payload.append(len(title))
        payload.extend(title)
        status = self.i2c.writeto(self.display_i2c, payload)

        #sleep(0.20)
        for i in range(6):
            self.update_line(i+1, slow=False)

    # end refresh_display ---------------------------------------------

    def update_line(self, line, data=None, slow=False):
        wmode = self.buttons[0] # waveforms
        fmode = self.buttons[2] # frequencies
        pmode = self.buttons[4] # parameters (amplitude, offset, parm1, parm2, parm3)
        #if self.dac.active: wmode = 0

        label = Widget.DISPLAY_LABELS[line- 1] # default label
        extra = ''

        # get some helper data to simplify selection below
        if   wmode == 0: w = self.dac.wave
        elif wmode == 1: w = self.dac.wave.fm
        else:            w = self.dac.wave.am
        postfix = ['(W)', '(F)', '(A)'][wmode]

        # try to determine the correct data to use
        if line >= 1 and line <= 3:	# wave names and multipliers
            if line == 1:       # waveform
                data = Widget.WAVEFORM_NAMES[self.dac.wave.type]
                extra = self.dac.wave.multiplier
            elif line == 2:     # mix
                data = Widget.WAVEFORM_NAMES[self.dac.wave.fm.type]
                extra = self.dac.wave.fm.multiplier
            elif line == 3:     # mix
                data = Widget.WAVEFORM_NAMES[self.dac.wave.am.type]
                extra = self.dac.wave.am.multiplier
            #extra = '{:%2d}'.format(extra) # right justify the numbers
        elif line == 4:     # frequency
            data = self.dac.__frequency_actual if (self.dac.active and not self.dirty) else self.frequency
            data = '{:.2f}'.format(data)
            extra = 'Hz'
        elif line == 5:     # phase (based on mode ... ugh)
            if self.dac.active:
                data = self.dac.wave.phase
            else:
                data = w.phase
                label = 'Phase' + postfix
            data = int(data * 100)   # convert to an integer percent
            extra = '%'
        elif line == 6: # status or parameters (based on mode ... ugh)
            extra = ''
            if pmode == 0: # amplitude
                data = w.amplitude
                label = 'Amplitude' + postfix
            elif pmode == 1:
                data = w.offset
                label = 'Offset' + postfix
            elif pmode == 2:
                data = w.properties[0]
                label = 'Property1' + postfix
            elif pmode == 3:
                data = w.properties[1]
                label = 'Property2' + postfix
            else:
                data = w.properties[2]
                label = 'Property3' + postfix
            # kludge to deal with 0.01 becoming 0.00999999999
            data += 0.001
            data = '{:.2f}'.format(data)

        # turns out we need to not update too quickly
        extra = ' ' + str(extra)

        payload = bytearray ()
        payload.append(Widget.DISPLAY_CMD_TEXT)
        payload.append(1+line)	# skip the first line which is used for the title
        text = ' {:11}{:>11s}{:3s}'.format(label, str(data), extra)   # the 'more' uses alt labels
        payload.append(len(text))
        payload.extend(text)
        status = self.i2c.writeto(self.display_i2c, payload)
        #print("display(%02X) line:%d is '%s'" % (self.display_i2c, line, text))

        if slow:
            sleep(0.20)

    # end update_line -------------------------------------------------



    def update_colors(self):    # change encoder colors based on the mode (base wave, mix wave, or envelope wave)
        wmode = self.buttons[0] # waveforms
        fmode = self.buttons[2] # frequencies
        pmode = self.buttons[4] # parameters (amplitude, offset, parm1, parm2, parm3)
        #GREEN = [0x00, 0xA0, 0x1F]  BLUE = [0x00, 0x1F, 0xFF]   RED = [0xA0, 0x1F, 0x1F]    YELLOW = [0xA0, 0xBF, 0x00]   WHITE = [0xA0, 0xA0, 0xA0]

        if wmode == 0:  # base waveform
            # mode: base waveform  colors(1,2,3,4): blue, yellow, green, red
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 1, 0x00, 0x0F, 0xFF, 0x00, 0x3F, 0xFF]))   # encoder 1: wave
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 2, 0xA0, 0xBF, 0x00, 0xE0, 0xFF, 0x00]))   # encoder 2: multiplier
            if fmode == 0:      # frequency 1s
                self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 3, 0x00, 0x3F, 0x00, 0xA0, 0xA0, 0xA0]))   # frequency 1
            elif fmode == 1:    # frequency 100s
                self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 3, 0x0F, 0x4F, 0x0F, 0xA0, 0xA0, 0xA0]))   # frequency 100
            else:               # frequency 10000s
                self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 3, 0x1F, 0x5F, 0x1F, 0xA0, 0xA0, 0xA0]))   # frequency 10000
        elif wmode == 1:# mix waveform
            # mode: mix waveform  colors(1,2,3,4): blue, yellow, green, red
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 1, 0x00, 0x3F, 0xFF, 0x00, 0x5F, 0xFF]))   # encoder 1: mix
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 2, 0xA0, 0xBF, 0x00, 0xE0, 0xFF, 0x00]))   # encoder 2: multiplier
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 3, 0x01, 0x01, 0x01, 0x0F, 0x0F, 0x0F]))   # encoder 3: n/a
        else:           # envelope waveform
            # mode: mix waveform  colors(1,2,3,4): blue, yellow, green, red
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 1, 0x00, 0x7F, 0xFF, 0x00, 0xAF, 0xFF]))   # encoder 1: mix
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 2, 0xA0, 0xBF, 0x00, 0xE0, 0xFF, 0x00]))   # encoder 2: multiplier
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 3, 0x01, 0x01, 0x01, 0x0F, 0x0F, 0x0F]))   # encoder 3: n/a

        self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 4, 0xA0, 0x0F, 0x0F, 0xFF, 0x1F, 0x1F]))   # encoder 4: phase
        if pmode == 0:      # amplitude
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 5, 0x1F, 0x1F, 0x1F, 0xAF, 0x7F, 0x7F]))   # encoder 5: param 1
        elif pmode == 1:    # offset
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 5, 0x3F, 0x3F, 0x3F, 0xAF, 0x7F, 0x7F]))   # encoder 5: param 1
        elif pmode == 2:    # param #1
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 5, 0x5F, 0x5F, 0x5F, 0xAF, 0x7F, 0x7F]))   # encoder 5: param 1
        elif pmode == 3:    # param #2
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 5, 0x7F, 0x7F, 0x7F, 0xAF, 0x7F, 0x7F]))   # encoder 5: param 1
        else:               # param #3
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_LED, 5, 0x9F, 0x9F, 0x9F, 0xAF, 0x7F, 0x7F]))   # encoder 5: param 1

    # end update_colors -----------------------------------------------



    def process(self):
        key_event = bytearray(2)
        encoder = bytearray(1)

        # ------------------------------------------------------------------------------------------------
        # process encoder devices
        # ------------------------------------------------------------------------------------------------

        #process buttons on encoder
        more = True
        while more:
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_BUTTON]))
            self.i2c.readfrom_into(self.encoder_i2c, key_event)
            if key_event[0] == 0:
                more = False
            else:
                k = key_event[0] # key number
                p = key_event[1] # True if this is a 'press' event
                # print("encoder device%d button%2d has been %s" % (a, k, ("pressed" if p else "released")))
                if p: # pressed
                    if k == 1:      # waveforms: base, mix, envelope
                        self.buttons[0] = (self.buttons[0] + 1) % 3
                        self.update_colors()    # change encoder colors based on the modes
                        self.update_line(5, slow=True)
                        self.update_line(6, slow=True)
                        self.dirty = True
                    elif k == 2:    # multiplier, no button action
                        pass
                    elif k == 3:    # frequency: 1s, 100s, 10000s
                        self.buttons[2] = (self.buttons[2] + 1) % 3
                        self.update_colors()    # change encoder colors based on the modes
                    elif k == 4:    # phase used to start/update or stop
                        #print("start/stop the corresponding dac")
                        if self.dac:
                            if self.dac.active:
                                if not self.dirty or self.frequency == 0:
                                    print("stopping DAC%d" % (self.dac_num))
                                    self.dac.stop()
                                    self.refresh_display(extra=' ')
                                else:
                                    self.dirty = False
                                    self.dac.start(self.frequency)
                                    print("restarting DAC%d at frequency %d (%8.2f)" % (self.dac_num, self.dac.__frequency, self.dac.__frequency_actual))
                                    self.update_line(4)
                            else:
                                if self.frequency:
                                    self.dac.start(self.frequency)
                                    print("starting DAC%d at frequency %d (%8.2f)" % (self.dac_num, self.dac.__frequency, self.dac.__frequency_actual))
                                    self.dirty = False
                                    self.update_line(4)
                                    self.refresh_display(extra='*')
                                else:
                                    print("cannot start DAC%d with a frequency of 0Hz" % (self.dac_num))

                        else:
                            print("error creating DAC%d" % (self.dac_num))
                        self.dirty = False
                    elif k == 5:      # parameters
                        self.buttons[4] = (self.buttons[4] + 1) % 5
                        self.update_colors()    # change encoder colors based on the modes
                        self.update_line(6)
                # update state

        # process encoders on device
        for i in range(5):
            self.i2c.writeto(self.encoder_i2c, bytes([Widget.ENCODER_CMD_DELTA, i+1]))
            self.i2c.readfrom_into(self.encoder_i2c, encoder)
            d = encoder[0]

            if  d:
                # d is a delta +/- 127
                if d > 127: d -= 256
                # print("device%d encoder%d: %3d" % (a, i, d))
                self.encoders_last[i] += d

                wmode = self.buttons[0] # waveforms
                fmode = self.buttons[2] # frequencies
                pmode = self.buttons[4] # parameters (amplitude, offset, parm1, parm2, parm3)
                #if self.dac.active: wmode = 0

                # get some helper data to simplify selection below
                if   wmode == 0: w = self.dac.wave
                elif wmode == 1: w = self.dac.wave.fm
                else:            w = self.dac.wave.am

                self.dirty = True

                # update variables
                if i == 0:  # encoder 1 = waveform : based on the wave mode, this changes the base wave, mix, or envelope
                    line = wmode + 1    # the wmode (0..2) is which waveform we are working with and the display is (1..3)
                    w.type = ((w.type + d) % Waveform.MAX_TYPE) if d > 0 else ((w.type + (Waveform.MAX_TYPE + d)) % Waveform.MAX_TYPE)
                    w.set(w.type)
                    self.update_line(line, slow=True)  # update waveform name
                    self.update_line(5, slow=True)     # update the phase to the new waveform
                    self.update_line(6, slow=True)     # update the parameters to the new waveform

                elif i == 1:  # encoder 2 = multiplier : based on the wave mode, this changes the base wave, mix, or envelope
                    line = wmode + 1    # the wmode (0..2) is which waveform we are working with and the display is (1..3)
                    w.multiplier += d
                    if w.multiplier < 1: w.multiplier = 1
                    self.update_line(line)

                elif i == 2: # encoder 3 = frequency : based on the frequency mode, this changes frequency by 1s, 100, or 10000s
                    if fmode == 0: # frequency 1
                        self.frequency += d
                    elif fmode == 1: # frequency 100
                        self.frequency += (d * 100)
                    else: # frequency 10000
                        self.frequency += (d * 10000)
                    if self.frequency < 0: self.frequency = 0
                    self.update_line(4)

                elif i == 3: # phase
                    w.phase += (d * 0.01)
                    w.phase = clamp(w.phase, -1.0, 1.0)
                    self.update_line(5)
                    if self.dac.active:
                        # we can dynamically change frequency of an active dac
                        self.dac.shift_phase(w.phase)

                else:   # parameters and properties
                    if pmode == 0: # amplitude
                        w.amplitude += (d * 0.01)
                        w.amplitude = clamp(w.amplitude, 0.0, 1.0)
                    elif pmode == 1:
                        w.offset += (d * 0.01)
                        w.offset = clamp(w.offset, 0.0, 1.0)
                    elif pmode == 2:
                        w.properties[0] += (d * 0.01)
                        w.properties[0] = clamp(w.properties[0], 0.0, 1.0)
                    elif pmode == 3:
                        w.properties[1] += (d * 0.01)
                        w.properties[1] = clamp(w.properties[1], 0.0, 1.0)
                    else:
                        w.properties[2] += (d * 0.01)
                        w.properties[2] = clamp(w.properties[2], 0.0, 1.0)
                    self.update_line(6)

    pass

    # end process -----------------------------------------------------

# end Widget Class

from machine import Pin
led = Pin(25, Pin.OUT)

led.on()

widgets0 = None
widgets1 = None

try:
    widgets0 = Widget(0)
    widgets1 = Widget(1, widgets0.i2c)

    # a bit hacky but ...
    widgets0.buttons[2] = 1 # frequencies defaults to 100s
    widgets1.buttons[2] = 1 # frequencies defaults to 100s

    print("welcome to awg2chan. please enter a command.")

    while (1):
        widgets0.process()
        widgets1.process()


except Exception as e:
    for i in range(8):
        led.toggle()
        sleep(0.25)
    led.on()
    print("Exception:")
    print_exception(e)
    if widgets0:
        widgets0.exit_handler()
    if widgets1:
        widgets1.exit_handler()

finally:
    led.off()
    print("Cleanup:")
    if widgets0:
        widgets0.exit_handler()
        del(widgets0)
    if widgets1:
        widgets1.exit_handler()
        del(widgets1)
