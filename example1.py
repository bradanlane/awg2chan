''' ************************************************************************************
* File:    awg2chan example1.py
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
*
* Bradán Lane - 2023.10.24
*
* ************************************************************************************ '''


''' ---
### Example 1

This example creates a single channel sine wave and changes it frequency through 
a range. The purpose of this example is to both demonstrate basic usage of the
`awg2chan` API and to check the accuracy of the generated sine wave.

**NOTE:** this example was tested using hardware where the first R2R DAC starts at pin=2

--- '''

# frequency range example

from time import sleep
from awg2chan import Waveform, DacChannel

try:
    print("starting setup ...")
    dac0 = DacChannel(0, pin=2, debug=True)
    # dac0.debug = True # debug information can also be turned on/off at any point
    dac0.wave.set(Waveform.SINE, None)
    print("... setup complete")

    frequency = 5021
    dac0.start(frequency)
    sleep(5.0)

    while (1):
        frequency += 300
        if frequency > 10000:
            frequency = 521
        dac0.change_frequency(frequency)
        sleep(2.0)    # we could skip the sleep delay because the frequency recalculation is so slow

except Exception as e:
    print("Exception:")
    print_exception(e)
    if dac0:
        dac0.exit_handler()

finally:
    print("Final:")
    if dac0:
        dac0.exit_handler()
        del dac0
