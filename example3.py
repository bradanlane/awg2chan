''' ************************************************************************************
* File:    awg2chan example3.py
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
*
* Bradán Lane - 2023.10.24
*
* ************************************************************************************ '''


''' ---
### Example 3

This example creates a USB serial interface and command set
for dynamically interacting with the `awg2chan` API.

**IMPORTANT:** this example uses the `stringscmd` library.
_see below_

**NOTE:** this example was tested using hardware where 
the first R2R DAC starts at pin=2, the second DAC starts at pin=10,
and the second DAC is wired in reverse order.

--- '''

# using USB Serial to command awg2chan

import sys
import select

from stringcmds import StringCommand

cmd = StringCommand()

try:
    print("welcome to awg2chan. please enter a command.")

    while (1):
        if select.select([sys.stdin],[],[],0)[0]:
            cmd.process(sys.stdin.readline())
        sleep (0.1)

except Exception as e:
    print("Exception:")
    sys.print_exception(e)
    cmd.exit_handler()

finally:
    print("Cleanup:")
    del cmd
