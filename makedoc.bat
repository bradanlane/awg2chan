echo off
REM customize this file to include the important source CPPs in the order they will render into the README.
REM In most cases main.cpp will provide the introduction and overview of the project.
REM pcregrep is used to find content between "''' ---" and "--- '''" and append the inclusive content into README.md
REM  The inclusive content should use standard markdown however, GitHub, GitLab, and BitBucket may have some additional restrictions

files\pcregrep.exe -M -h -o1  "''' ---\n((\n|.)*?)--- '''" .\awg2chan.py > README.md
files\pcregrep.exe -M -h -o1  "''' ---\n((\n|.)*?)--- '''" example1.py >> README.md
files\pcregrep.exe -M -h -o1  "''' ---\n((\n|.)*?)--- '''" example2.py >> README.md
files\pcregrep.exe -M -h -o1  "''' ---\n((\n|.)*?)--- '''" example3.py >> README.md
files\pcregrep.exe -M -h -o1  "''' ---\n((\n|.)*?)--- '''" stringcmds.py >> README.md