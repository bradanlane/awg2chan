''' ************************************************************************************
* File:    string command interface (stringcmds.py) for awg2chan 
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
*
* Bradán Lane - 2023.10.24
*
* ************************************************************************************ '''


''' ---
--------------------------------------------------------------------------
## StringCommand

A string interface to the `awg2chan` arbitrary waveform generator.
This library is typically used in conjunction with a USB Serial read/write
service.

The library is used by one of the examples for `awg2chan`.

--- '''

from awg2chan import Waveform, DacChannel

''' ---
--------------------------------------------------------------------------
### StringCommand Class

The `StringCommand` class provides a command string interface for `awg2chan`.

It was created as a demonstration for interactive with `awg2chan` using a USB serial connection.


--- '''

class StringCommand:

    HELP = """

    awg2chan Commands:

    Note: a list with colons indicates use one of the elements in the command
            all commands and parameters are 'space delimited'
            do not use spaces in list parameters

    help - this content

    create <num> [<pin> <size> <reversed>] - create a DacChannel
        'num' is one of 0:1
        'pin' is an integer value for the starting GPIO of the R2R
        'size' is an integer value for sampling buffer size (must be even multiple of 4)
        'reversed' is one of 0:1 (boolean)
        eg: create 0 2 1024 0

    set <num> <type> [<parameter(s)>] - set the base wave for a DacChannel
        'num' is one of 0:1
        'type' is one of sine:asd:square:sawtooth1:sawtooth2:triangle:sinc:gaussian:exponential:noise
        'parameter(s)' represent a single value or a list (comma separated with no whitespace)
        eg: set 0 sine
        eg: set 0 asd 0.25,0.50,0.25

    start <num> <frequency> - start a DacChannel at the frequency
        'num' is one of 0:1
        'frequency' is an integer (with no formatting)
        eg: start 0 880

    stop <num> - stop a DacChannel
        eg: stop 0

    frequency <num> <frequency> - change the frequency of the DacChannel
        'num' is one of 0:1
        'frequency' is an integer (with no formatting)
        eg: frequency 0 1760

    phase <num> <shift> - shift the phase of the wave of the DacChannel
        'num' is one of 0:1
        'shift' is a value between -1.0 and 1.0
        eg: phase 0 -0.5

    delete <num>
        'num' is one of 0:1
        eg: delete 0

    """

    '''
    # potential future features:
    # expose amplitude and frequency modifiers
    fm <num> <type> [<parameter(s)>] - set the frequency modifier wave to the base wave for a DacChannel
        'num' is one of 0:1
        'type' is one of sine:asd:square:sawtooth1:sawtooth2:triangle:sinc:gaussian:exponential:noise
        'parameter(s)' represent a single value or a list (comma separated with no whitespace)
        eg: mix 0 sine

    am <num> <type> [<parameter(s)>] - set amplitude modifier wave to the base wave for a DacChannel
        'num' is one of 0:1
        'type' is one of sine:asd:square:sawtooth1:sawtooth2:triangle:sinc:gaussian:exponential:noise
        'parameter(s)' represent a single value or a list (comma separated with no whitespace)
        eg: envelope 0 asd 0.25,0.50,0.25
    '''

    WAVE_NAMES = ["none", "sine", "asd", "square", "sawtooth1", "sawtooth2", "triangle", "gaussian", "sinc", "exponential", "noise"]

    ''' ---
--------------------------------------------------------------------------
#### StringCommand Constructor
The constructor for the `StringCommand` class takes no arguments
    --- '''
    def __init__(self):
        self.__dac = {}
        self.__dac[0] = None
        self.__dac[1] = None


    ''' ---
#### StringCommand Destructor
The destructor for `StringCommand` insures the DacChannel objects have an opportunity to stop and perform any necessary cleanup.
    --- '''
    def __del__(self):
        self.exit_handler()

    def exit_handler(self):
        if (self.__dac[0]):
            self.__dac[0].exit_handler()
            del self.__dac[0]
            self.__dac[0] = None
        if (self.__dac[1]):
            self.__dac[0].exit_handler()
            del self.__dac[1]
            self.__dac[1] = None


    ''' ---
#### process() Method
The `process()` method takes one parameter
- `command_string` - a string formatted according to the supported commands detailed in the `help` text.
    --- '''
    def process(self, command):

        parts = command.lower().split()
        count = len(parts)
        #print("received: ", parts)

        # ---------------------------------------------------------------------
        # HELP
        # ---------------------------------------------------------------------
        if parts[0] == 'help':
            print(StringCommand.HELP)
            return

        if count < 2:
            print("insufficient parameters")
            print("all commands require the DAC number")
            print("you can enter 'help' for a list of all available commands")
            return
        
        if not ((parts[1] == '0') or (parts[1] == '1')):
            print("incorrect parameter")
            print("the first parameter must be the integer DAC number 0 or 1")
            return

        d = int(parts[1])   # the dac number parameter is common to all the user input

        # ---------------------------------------------------------------------
        # CREATE
        # ---------------------------------------------------------------------
        if parts[0] == 'create':
            # create <num> [<pin> <size> <reversed>] - create a DacChannel
            if count < 2:
                print("insufficient parameters")
                print("usage: create <num> [<pin> <size> <reversed>] - create a DacChannel")
                return
            p = 0
            s = 512
            r = False
            g = False
            if count > 2: p = int(parts[2])
            if count > 3: s = int(parts[3])
            if count > 4: r = True if int(parts[4]) else False
            if count > 5: g = True if int(parts[5]) else False

            if self.__dac[d]:
                # we are replacing an existing DacChannel
                del self.__dac[d]
                self.__dac[d] = None
            self.__dac[d] = DacChannel(d, pin=p, size=s, reversed=r, debug=g)
            print("created DAC%d on pin %d%s with %d samples" % (d, p, (" (reversed)" if r else "" ), s))
            return

        # ---------------------------------------------------------------------
        if not self.__dac[d]:
            print("incorrect parameter")
            print("attempting to use DAC%d which has not been created" %(d))
            return

        # ---------------------------------------------------------------------
        # SET
        # ---------------------------------------------------------------------
        if parts[0] == 'set':
            # set <num> <type> [<parameter(s)>] - set the base wave for a DacChannel
            if count < 3:
                print("insufficient parameters")
                print("usage: set <num> <type> [<parameter(s)>] - set the base wave for a DacChannel")
                return
            try:
                w = StringCommand.WAVE_NAMES.index(parts[2])
            except ValueError as e:
                print("unknown type: %s" % (parts[2]), e)
                print("supported waveform types: ", StringCommand.WAVE_NAMES)
                return
            params = None
            if count > 3:
                params = parts[3].split(';')        # mak a list of the parameters
                params = list(map(float, params))   # convert the list of stings to a list of floats
            if self.__dac[d].frequency:
                # the dac is active, stop it
                self.__dac[d].stop()
            self.__dac[d].wave.set(w, params)
            print("set DAC%d waveform to %s with %s parameters" % (d, StringCommand.WAVE_NAMES[w], ('['+parts[3]+']' if params else "no")))
            return

        # ---------------------------------------------------------------------
        # START
        # ---------------------------------------------------------------------
        if parts[0] == 'start':
            # start <num> <frequency> - start a DacChannel at the frequency
            if count < 3:
                print("insufficient parameters")
                print("start <num> <frequency> - start a DacChannel at the frequency")
                return
            f = int(parts[2])
            if (f < 10) or (f > 1000000):
                print("invalid frequency")
                print("usable frequency range is 10Hz to 1MHz (represented as an integer)")
                return
            self.__dac[d].start(f)
            print("start DAC%d with a frequency of %d" % (d, f))
            return

        # ---------------------------------------------------------------------
        # STOP
        # ---------------------------------------------------------------------
        if parts[0] == 'stop':
            # stop <num> - stop a DacChannel
            self.__dac[d].stop()
            print("stop DAC%d" % (d))
            return

        # ---------------------------------------------------------------------
        # FREQUENCY
        # ---------------------------------------------------------------------
        if parts[0] == 'frequency':
            # frequency <num> <frequency> - change the frequency of the DacChannel
            if count < 3:
                print("insufficient parameters")
                print("frequency <num> <frequency> - change the frequency of the DacChannel")
                return
            f = int(parts[2])
            if (f < 10) or (f > 1000000):
                print("invalid frequency")
                print("usable frequency range is 10Hz to 1MHz (represented as an integer)")
                return
            self.__dac[d].change_frequency(f)
            print("DAC%d frequency changed to %d" % (d, f))
            return

        # ---------------------------------------------------------------------
        # PHASE
        # ---------------------------------------------------------------------
        if parts[0] == 'phase':
            # phase <num> <offset> - shift the phase of the wave of the DacChannel
            if count < 3:
                print("insufficient parameters")
                print("phase <num> <offset> - shift the phase of the wave of the DacChannel")
                return
            s = float(parts[2])
            if (s < -1.0) or (s > 1.0):
                print("invalid frequency")
                print("the phase shift cannot exceed 100% (aka a value of 1.0))")
                return
            self.__dac[d].shift_phase(s)
            print("DAC%d waveform phase shifted %.2f" % (d, s))
            return

        # ---------------------------------------------------------------------
        # UNKNONW
        # ---------------------------------------------------------------------
        print("unrecognized command")
        print("type 'help' for available commands and their syntax")


# -------------------------------------------------------------------------------

